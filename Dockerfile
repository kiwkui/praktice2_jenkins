FROM python:3.9-slim

WORKDIR /app

COPY . .

CMD ["python", "python_program.py", "$SEARCH_VALUE"] 