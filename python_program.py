﻿import random
import sys
import os

def generate_sorted_array(size):
    return sorted([random.randint(1, 1000) for _ in range(size)])

def binary_search(array, target):
    left, right = 0, len(array) - 1
    while left <= right:
        mid = (left + right) // 2
        if array[mid] == target:
            return mid
        elif array[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return -1

if __name__ == "__main__":
    search_value = os.environ.get('SEARCH_VALUE')
    try:
        target = int(search_value)
    except ValueError:
        print("Пожалуйста, введите целое число.")
        sys.exit(1)

    array = generate_sorted_array(100)
    print("Сгенерированный массив:", array)

    index = binary_search(array, target)
    if index != -1:
        print(f"Искомое значение {target} найдено на позиции {index}.")
    else:
        print(f"Искомое значение {target} не найдено в массиве.")
